﻿using Escape.Model;
using Escape.Persistence;
using Escape.WPF.ViewModel;
using Escape.WPF.View;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using System.Linq.Expressions;
using System.ComponentModel;

namespace Escape.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, IDisposable
    {
        #region Fields

        private EscapeGameModel _model = null!;
        private EscapeViewModel _viewModel = null!;
        private MainWindow _view = null!;
        private WelcomeWindow _welcomeWindow = null!;
        private WelcomeViewModel _welcomeViewModel = null!;
        #endregion

        #region Constructor

        public App()
        {
            Startup += new StartupEventHandler(App_Startup);
        }

        #endregion

        #region Application event handlers

        private void App_Startup(object? sender, StartupEventArgs e) //the welcome window opens first
        {
            WelcomeWindow();
        }

        //instancing a model with the appropriate size
        private void WelcomeViewModel_ModelSet11(object? sender, EventArgs e)
        {
            _model = new EscapeGameModel(TableSize.ELEVEN, new EscapeFileDataAccess());
            CommonSettings();
        }

        private void WelcomeViewModel_ModelSet15(object? sender, EventArgs e)
        {
            _model = new EscapeGameModel(TableSize.FIFTEEN, new EscapeFileDataAccess());
            CommonSettings();
        }
        private void WelcomeViewModel_ModelSet21(object? sender, EventArgs e)
        {
            _model = new EscapeGameModel(TableSize.TWENTYONE, new EscapeFileDataAccess());
            CommonSettings();
        }

        private void CommonSettings() //the settings which always has to be adjusted
        {
            _model.GameOver += new EventHandler<EscapeEventArgs>(Model_GameOver); //subscribing to the event of the game over
            _model.NewGame(); //starting a new game

            //instancing a new viewmodel, and subscribing to the command events
            _viewModel = new EscapeViewModel(_model);
            _viewModel.NewGame += new EventHandler(ViewModel_NewGame);
            _viewModel.ChangeTableSize += new EventHandler(ViewModel_ChangeTableSize);
            _viewModel.LoadGame += new EventHandler(ViewModel_LoadGame);
            _viewModel.SaveGame += new EventHandler(ViewModel_SaveGame);
            _viewModel.ExitGame += new EventHandler(ViewModel_ExitGame);

            //opening the view of the game
            _view = new MainWindow();
            _view.DataContext = _viewModel;
            _view.Closing += new CancelEventHandler(View_Closing);
            _welcomeWindow.Close(); //closing welcomewindow
            _welcomeWindow = null!;
            _view.Show();
        }

        private void ViewModel_NewGame(object? sender, EventArgs e) //new game command settings
        {
            if (_model.IsGameOver || MessageBox.Show("Are you sure you want to start a new game?" + Environment.NewLine + "Unsaved datas will be lost.", "Escape Game", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                _model.NewGame();
            }
        }

        private void ViewModel_ChangeTableSize(object? sender, EventArgs e) //changing table size command settings
        {
            if (_model.IsGameOver || MessageBox.Show("Are you sure you want to change table size?" + Environment.NewLine + "Unsaved datas will be lost.", "Escape Game", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                WelcomeWindow();
                _view.Close();
            }
        }

        private async void ViewModel_LoadGame(object? sender, EventArgs e) //loading game command settings
        {
            EscapeGameModel modelLoad = _model;
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Loading Escape Table";
                if (openFileDialog.ShowDialog() == true)
                {
                    await modelLoad.LoadGameAsync(openFileDialog.FileName);
                    _model = modelLoad;
                }
            }
            catch (EscapeDataException)
            {
                MessageBox.Show("Loading game was unsuccessful!" + Environment.NewLine + "Not appropriate path or fileformat!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Loading game was unsuccessful!" + Environment.NewLine + "Couldn't match the selected file's table size to the originally selected table size." + Environment.NewLine + "Go back to the main menu and select another size of the table.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void ViewModel_SaveGame(object? sender, EventArgs e) //saving game command settings
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Loading Escape Table";
                if (saveFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        await _model.SaveGameAsync(saveFileDialog.FileName);
                    }
                    catch (EscapeDataException)
                    {
                        MessageBox.Show("Saving game was unsuccessful!" + Environment.NewLine + "Not appropriate path, or the library is readonly.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Saving file was unsuccessful!", "Escape", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void ViewModel_ExitGame(object? sender, EventArgs e) //exit game command settings
        {
            _view.Close(); //calls View_Closing method automatically
        }

        private void View_Closing(object? sender, CancelEventArgs e) //what happens if we close the window
        {
            if (_welcomeWindow is not null)
            {
                if (_model != null)
                    _model.Dispose();

                return;
            }

            // Call the PauseGameCommand to pause the game if it's not over
            if (!_model.IsGameOver)
            {
                if (_model.IsTimerEnabled())
                    _viewModel.PauseGameCommand.Execute(null);

                if (MessageBox.Show("Are you sure you want to leave?" + Environment.NewLine + "Unsaved datas will be lost.", "Escape Game", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    e.Cancel = true;
            }

            if (!(e.Cancel) && _model != null)
                _model.Dispose();
        }

        private void Model_GameOver(object? sender, EscapeEventArgs e) //what happens if the game is over
        {
            if (e.IsWon)
            {
                MessageBox.Show("You Won!" + Environment.NewLine +
                                "Your playtime was " +
                                TimeSpan.FromSeconds(e.GameTime).ToString("g"), "Escape Game",
                                MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
            }
            else
            {
                MessageBox.Show("You Lost!",
                                "Escape Game",
                                MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
            }

            //Setting Pause button to game over mode
            Application.Current.Dispatcher.Invoke(() =>
            {
                _viewModel.PauseGameCommand.Execute(null);
            });
            
        }
        #endregion

        #region Private methods
        private void WelcomeWindow() //the settings of the welcome window
        {
            _welcomeWindow = new WelcomeWindow();
            _welcomeViewModel = new WelcomeViewModel();
            _welcomeWindow.DataContext = _welcomeViewModel;

            _welcomeViewModel.modelSetSize11 += WelcomeViewModel_ModelSet11;
            _welcomeViewModel.modelSetSize15 += WelcomeViewModel_ModelSet15;
            _welcomeViewModel.modelSetSize21 += WelcomeViewModel_ModelSet21;
            _welcomeWindow.Show();
        }

        #endregion

        #region IDisposable implementation

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _model?.Dispose();
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
