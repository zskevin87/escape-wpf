﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.WPF.ViewModel
{
    public class EscapeField : ViewModelBase
    {
        #region Fields

        //the traits of each field
        private Boolean _isMine;
        private Boolean _isPlayer;
        private Boolean _isEnemy;
        private String _text = String.Empty;

        #endregion

        #region Properties

        public Boolean IsMine //returns if there is a mine on the field
        {
            get { return _isMine; }
            set
            {
                if (_isMine != value)
                {
                    _isMine = value;
                    OnPropertyChanged();
                }
            }
        }

        public Boolean IsPlayer //returns if the player is on the field
        { 
            get { return _isPlayer; }
            set
            {
                if (_isPlayer != value)
                {
                    _isPlayer = value; 
                    OnPropertyChanged();
                }
            }
        }

        public Boolean IsEnemy //returns if an enemy is on the field
        {
            get { return _isEnemy; }
            set
            {
                if (_isEnemy != value)
                {
                    _isEnemy = value;
                    OnPropertyChanged();
                }
            }
        }

        public String Text //returns the text of the field ( X if it is mine, elseway it is empty)
        {
            get { return _text; }
            set
            {
                if (_text != value)
                {
                    _text = value;
                    OnPropertyChanged();
                }
            }
        }

        public int X { get; set; } //returns the coordinate of the field on the X axis
        public int Y { get; set; } //returns the coordinate of the field on the Y axis

        public Point XY //returns the coordinates of the field
        {
            get { return new Point(X, Y); }
        }
        #endregion
    }
}
