﻿using Escape.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Drawing;

namespace Escape.WPF.ViewModel
{
    public class EscapeViewModel : ViewModelBase
    {
        #region Fields
        //using a model instance to exchange information
        private EscapeGameModel _model;

        #endregion

        #region Properties
        // commands of the menu
        public DelegateCommand NewGameCommand { get; private set; }

        public DelegateCommand ChangeTableSizeCommand { get; private set; }
        
        public DelegateCommand LoadGameCommand { get; private set; }

        public DelegateCommand SaveGameCommand { get; private set; }

        public DelegateCommand ExitGameCommand { get; private set; }

        public DelegateCommand PauseGameCommand { get; private set; }

        public ObservableCollection<EscapeField> Fields { get; set; } //the list which contains all fields

        public string PauseText { get; private set; }

        //stepping commands (arrow keys on the keyboard) which tell the model to step the player
        public DelegateCommand StepUp { get; private set; }
        public DelegateCommand StepDown { get; private set; }
        public DelegateCommand StepLeft { get; private set; }
        public DelegateCommand StepRight { get; private set; }

        //public EscapeGameModel Model { get { return _model; } }

        public int TableSize { get { return _model.Table.Size; } }

        public String GameTime { get { return TimeSpan.FromSeconds(_model.GameTime).ToString("g"); } }

        #endregion

        #region Event Handlers

        public event EventHandler? NewGame;
        public event EventHandler? ChangeTableSize;
        public event EventHandler? LoadGame;
        public event EventHandler? SaveGame;
        public event EventHandler? ExitGame;

        #endregion

        #region Constructor

        public EscapeViewModel(EscapeGameModel model)
        {
            _model = model;

            //pairing the commands to eventhandlers
            _model.Stepping += new EventHandler<EscapeStepEventArgs>(Model_Stepping);
            _model.GameAdvanced += new EventHandler<EscapeEventArgs>(Model_GameAdvanced);
            _model.GameCreated += new EventHandler<EscapeEventArgs>(Model_GameCreated);

            NewGameCommand = new DelegateCommand(param => OnNewGame());
            ChangeTableSizeCommand = new DelegateCommand(param => OnChangeTableSize());
            LoadGameCommand = new DelegateCommand(param => OnLoadGame());
            SaveGameCommand = new DelegateCommand(param => OnSaveGame());
            ExitGameCommand = new DelegateCommand(param => OnExitGame());
            PauseGameCommand = new DelegateCommand(param => OnPauseGame());

            StepUp = new DelegateCommand(param => OnStepUp());
            StepDown = new DelegateCommand(param => OnStepDown());
            StepLeft = new DelegateCommand(param => OnStepLeft());
            StepRight = new DelegateCommand(param => OnStepRight());

            PauseText = "Pause Game";

            //adding the fields into the list
            Fields = new ObservableCollection<EscapeField>();
            for (int i = 0; i < _model.Table.Size; i++)
            {
                for (int j = 0; j < _model.Table.Size; j++)
                {
                    Fields.Add(new EscapeField
                    {
                        IsMine = false,
                        IsPlayer = false,
                        IsEnemy = false,
                        Text = String.Empty,
                        X = i,
                        Y = j
                    });
                }
            }

            RefreshTable();
        }

        #endregion

        #region Private methods

        private void RefreshTable() //checks the field traits at each field
        {
            foreach (EscapeField field in Fields)
            {
                field.IsPlayer = _model.Table.IsPlayer(field.X, field.Y);
                field.IsEnemy = _model.Table.IsEnemy(field.X, field.Y);
                field.IsMine = _model.Table.IsMine(field.X,field.Y);
                field.Text = _model.Table.IsMine(field.X, field.Y) ? "X" : String.Empty;
            }
        }
        private void OnPauseGame()
        {
            if (!_model.IsGameOver)
            {
                if (_model.IsTimerEnabled())
                {
                    PauseText = "Continue";
                    _model.StopTimer();
                }

                else
                {
                    PauseText = "Pause Game";
                    _model.StartTimer();
                }
            }
            else
            {
                _model.StopTimer();
                PauseText = "Game is Over";
            }

            OnPropertyChanged(nameof(PauseText));
        }

        #endregion

        #region Game event handlers

        private void Model_Stepping(object? sender, EscapeStepEventArgs e) //eventhandler of the steppings 
        {
            RefreshTable();
        }

        private void Model_GameAdvanced(object? sender, EscapeEventArgs e) //eventhandler of the game advancing 
        {
            OnPropertyChanged(nameof(GameTime));
        }

        private void Model_GameCreated(object? sender, EscapeEventArgs e) //eventhandler of the game creating
        {
            RefreshTable();
        }

        #endregion

        #region Controlling private methods

        //the command methods of the player controlling.
        //if the timer is not enabled, the game might be over or paused (player cannot move then)
        private void OnStepUp()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new Point(-1, 0));
        }

        private void OnStepDown()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new Point(1, 0));
        }

        private void OnStepLeft()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new Point(0, -1));
        }

        private void OnStepRight()
        {
            if (_model.IsTimerEnabled())
                _model.Step(new Point(0, 1));
        }

        #endregion

        #region Event methods

        private void OnNewGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            NewGame?.Invoke(this, EventArgs.Empty);

            if (_model.IsTimerEnabled())
            {
                PauseText = "Pause Game";
                OnPropertyChanged(nameof(PauseText));
            }
        }

        private void OnChangeTableSize()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            ChangeTableSize?.Invoke(this, EventArgs.Empty);
        }

        private void OnLoadGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            LoadGame?.Invoke(this, EventArgs.Empty);

            if (_model.IsTimerEnabled())
            {
                PauseText = "Pause Game";
                OnPropertyChanged(nameof(PauseText));
            }
        }

        private void OnSaveGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            SaveGame?.Invoke(this, EventArgs.Empty);
        }

        private void OnExitGame()
        {
            if (_model.IsTimerEnabled())
                OnPauseGame();

            ExitGame?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
