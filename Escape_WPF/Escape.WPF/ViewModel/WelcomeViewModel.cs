﻿using Escape.Model;
using Escape.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Escape.WPF.ViewModel
{

    public class WelcomeViewModel : ViewModelBase
    {
        #region Properties
        //the commands of the three buttons
        public DelegateCommand Size11 { get; private set; }
        public DelegateCommand Size15 { get; private set; }
        public DelegateCommand Size21 { get; private set; }

        #endregion

        #region Constructor

        public WelcomeViewModel()
        {
            //matching the commands to the appropriate events
            Size11 = new DelegateCommand(param => OnSize11());
            Size15 = new DelegateCommand(param => OnSize15());
            Size21 = new DelegateCommand(param => OnSize21());
        }

        #endregion

        #region Events

        //the events of the buttons
        public event EventHandler? modelSetSize11;
        public event EventHandler? modelSetSize15;
        public event EventHandler? modelSetSize21;

        #endregion

        #region Welcome Page Button methods

        //eventhandler methods
        private void OnSize11()
        {
            modelSetSize11?.Invoke(this, EventArgs.Empty);
        }
        private void OnSize15()
        {
            modelSetSize15?.Invoke(this, EventArgs.Empty);
        }
        private void OnSize21()
        {
            modelSetSize21?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
