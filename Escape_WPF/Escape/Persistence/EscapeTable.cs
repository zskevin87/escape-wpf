﻿using System.Drawing;
using System.Reflection.Metadata.Ecma335;

namespace Escape.Persistence
{
    public class EscapeTable
    {
        #region Fields
        private int[,] _fieldPlayer;
        //this contains the datas of the fields: each field can be empty (0) or the player can be on it (1)
        private int[,] _fieldEnemies;
        //this contains the datas of the fields: each field can be empty (0) or an enemy can be on it (2)
        private bool[,] _fieldMines;
        //there can be mines on the fields. True if there is a mine on a specific field.
        private Point _enemy1Coords;
        private Point _enemy2Coords;
        private int _loadedGameTime; //if we load a game, we will know the playtime when it was saved
        #endregion

        #region Properties
        public int Size { get { return _fieldPlayer.GetLength(0); } }
        // the size of the three fields has to be the same! Therefore it doesn't matter which length we ask for
        public int[,] GetFieldPlayer() { return _fieldPlayer; }
        public int[,] GetFieldEnemies() { return _fieldEnemies; }
        public bool[,] GetFieldMines() { return _fieldMines; }

        public Point PlayerCoordinates { get { return GetPlayerCoordinates(); } }
        //this property tells where the player is. The return value is a point (row value, column value)

        public Point Enemy1Coordinates { get { Point xy = _enemy1Coords; return xy; } private set { Point xy = value; _enemy1Coords = xy; } }
        public Point Enemy2Coordinates { get { Point xy = _enemy2Coords; return xy; } private set { Point xy = value; _enemy2Coords = xy; } }
        public int LoadedGameTime { get { int time = _loadedGameTime; return time; } private set { int time = value; _loadedGameTime = time; } }

        //this property tells where the enemies are. The return value is a list of points
        public List<Point> MinesCoordinates { get { return GetMinesCoordinates(); } }
        //this property tells where the mines are. The return value is a list of points

        public int this[int x, int y] { get { return GetValue(x, y); } }
        //returns a number that shows what is on a specific field
        //(0 - empty, 1 - player, 2 - enemy, 3 - the player and an enemy is on the same field)

        public int PlayingEnemiesCount { get { return CountEnemies(); } } 
        //only counts enemies who are not in mines
        public int MinesCount { get { return CountMines(); } }
        //counts the number of mines on the table
        #endregion

        #region Constructors
        public EscapeTable() : this(11) { }
        //the basic table size stands for 11x11 units
        public EscapeTable(int size)
        {
            if (size < 0)
                throw new ArgumentOutOfRangeException(nameof(size), "Table size cannot be less than 0.");

            _fieldPlayer = new int[size, size];
            _fieldEnemies = new int[size, size];
            _fieldMines = new bool[size, size];
        }
        //creating as large arrays (table) as it is given as an argument
        #endregion

        #region Public methods

        #region Basic methods
        public int GetValue(int x, int y) 
            //returns the value of the field (doesn't tell the locations of mines)
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            if (_fieldPlayer[x, y] == 1 && _fieldEnemies[x, y] == 2) { return 3; } 
            // 3 means that the player and an enemy is on the same field.
            else if (_fieldPlayer[x, y] == 1) { return 1; }
            else if (_fieldEnemies[x, y] == 2) { return 2; }
            return 0;
        }

        public bool IsEmpty(int x, int y) 
            //True if the field is empty (0) in both arrays and there is no any mines here
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            return _fieldPlayer[x, y] == 0 && _fieldEnemies[x, y] == 0 && !_fieldMines[x, y];
        }
        public bool IsPlayer(int x, int y) //True if the player is on the field  (1)
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            return _fieldPlayer[x, y] == 1;
        }
        public bool IsEnemy(int x, int y) //True if an enemy is on the field  (2)
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            return _fieldEnemies[x, y] == 2;
        }
        public bool IsMine(int x, int y) //True if a mine is on the field
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            return _fieldMines[x, y];
        }
        public int CountEnemies()
        //counts enemies on the table
        {
            int count = 0;
            for (int i = 0; i < _fieldEnemies.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldEnemies.GetLength(1); j++)
                {
                    if (_fieldEnemies[i, j] == 2) { count++; }
                }
            }
            return count;
        }

        public int CountMines()
            //counts the mines on the table
        {
            int count = 0;
            for (int i = 0; i < _fieldMines.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldMines.GetLength(1); j++)
                {
                    if (_fieldMines[i, j]) { count++; }
                }
            }
            return count;
        }

        public void SetLoadGameTime(int gametime) //if we load the game, this sets the current gametime
        {
            if (gametime < 0)
                throw new ArgumentOutOfRangeException(nameof(gametime), "Time cannot be less than 0");
            LoadedGameTime = gametime;
        }

        #endregion

        #region Spawner/Motion methods
        public void SetPlayer(int x, int y)
            //spawns the player onto the given field.
            //The method won't run if the given field is not empty or the player has already spawned
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");
            if (HasThePlayerAlreadySpawned())
                throw new InvalidOperationException("There is already a player on the table! Spawning was unsuccesful.");

            _fieldPlayer[x, y] = 1;
        }

        public void SetEnemy(int x, int y)
            //spawns an enemy on the given field
            //the method won't run if the given field is not empty
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");
            //should check if the count of enemies earned the max limit -> done, gamemodel only lets two enemies

            _fieldEnemies[x, y] = 2;
        }

        public void SetEnemyCoordinate(int enemynum, Point coordinate) //sets the coordinates of enemies as propeties
        {
            if (enemynum <= 0 || enemynum > 2)
                throw new ArgumentOutOfRangeException(nameof(enemynum), "There can be only 2 enemies on a table.");
            if (coordinate.X < 0 || coordinate.X >= Size || coordinate.Y < 0 || coordinate.Y >= Size)
                throw new ArgumentOutOfRangeException(nameof(coordinate), "The coordinate is out of the range of the table.");

            switch (enemynum)
            {
                case 1:
                    Enemy1Coordinates = coordinate; //sets the first enemy's coordinate
                    break;
                case 2:
                    Enemy2Coordinates = coordinate; //sets the second enemy's coordinate
                    break;
            }
        }

        public void SetMine(int x, int y)
            //spawns a mine on the given field
            //the method won't run if the given field is not empty
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            _fieldMines[x, y] = true;
        }

        public void SetEmpty(int x, int y)
        {
            //making a field empty
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");

            _fieldPlayer[x, y] = 0;
            _fieldEnemies[x, y] = 0;
            _fieldMines[x, y] = false;
        }

        public void StepPlayer(int x, int y, int newX, int newY)
            //makes the player value stepped to another given field (newX, newY) from where it was originally (x,y)
        {
            /* 
            There will be 4 clicking/key event methods which provide to step in the appropriate way (up,down,left,right)              
            In each method, there will be a try-catch section which catches the invalid stepping attempts and does nothing
            (e.c the player is in the first row and wants to step upper. 
            As it cannot step any futhter which is out of the territory of the table,
            the Stepping-Up button will do nothing in this case).
            */
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");
            if (newX < 0 || newX >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "NewX is out of range.");
            if (newY < 0 || newY >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "NewY is out of range.");
            if (!IsPlayer(x, y))
                throw new Exception("Player is not on this field, stepping was unsuccessful.");

            _fieldPlayer[x, y] = 0;
            _fieldPlayer[newX, newY] = 1;
        }

        public void StepEnemies()
            //make the enemy values stepped to another field.
            //won't run if there is no any enemies on the table, or the player has not spawned
            //enemies need to know where the player is to make their own stratergy to get to the player
            //if an enemy has fallen into a mine, it cannot step anymore.
            //if an enemy has stratergy to step onto a field...
                //but there is already an other enemy on the chosen one,...
                //the enemy skips stepping (waits until the field will be empty)
        {
            if (GetEnemyCoordinates().Length == 0)
                throw new InvalidOperationException("There is no any enemies on the table. Cannot find stratergy for no one.");
            if (!HasThePlayerAlreadySpawned())
                throw new InvalidOperationException("The player has not spawned yet. Cannot find stratergy to get to the player.");


            if (!IsAnEnemyInAMine(Enemy1Coordinates.X, Enemy1Coordinates.Y))
            {
                Point stratergy = EnemySteppingStratergy(Enemy1Coordinates);
                if (!(_fieldEnemies[Enemy1Coordinates.X + stratergy.X, Enemy1Coordinates.Y + stratergy.Y] == 2
                    && !IsMine(Enemy1Coordinates.X + stratergy.X, Enemy1Coordinates.Y + stratergy.Y)))
                //enemies can fall into the same mine (together), but cannot step to the same not mine field together!
                { 
                    _fieldEnemies[Enemy1Coordinates.X, Enemy1Coordinates.Y] = 0;
                    _fieldEnemies[Enemy1Coordinates.X + stratergy.X, Enemy1Coordinates.Y + stratergy.Y] = 2;
                    Enemy1Coordinates = new Point(Enemy1Coordinates.X + stratergy.X, Enemy1Coordinates.Y + stratergy.Y);
                }
            }

            if (!IsAnEnemyInAMine(Enemy2Coordinates.X, Enemy2Coordinates.Y))
            {
                Point stratergy = EnemySteppingStratergy(Enemy2Coordinates);
                if (!(_fieldEnemies[Enemy2Coordinates.X + stratergy.X, Enemy2Coordinates.Y + stratergy.Y] == 2
                    && !IsMine(Enemy2Coordinates.X + stratergy.X, Enemy2Coordinates.Y + stratergy.Y)))
                //enemies can fall into the same mine (together), but cannot step to the same not mine field together!
                {
                    _fieldEnemies[Enemy2Coordinates.X, Enemy2Coordinates.Y] = 0;
                    _fieldEnemies[Enemy2Coordinates.X + stratergy.X, Enemy2Coordinates.Y + stratergy.Y] = 2;
                    Enemy2Coordinates = new Point(Enemy2Coordinates.X + stratergy.X, Enemy2Coordinates.Y + stratergy.Y);
                }
            }
        }
        #endregion

        #region Querries

        //Character Coordinate Querries:
        public bool HasThePlayerAlreadySpawned() //checks if the player has spawned 
        {
            for (int i = 0; i < _fieldPlayer.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldPlayer.GetLength(1); j++)
                {
                    if (_fieldPlayer[i, j] == 1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Point GetPlayerCoordinates() //gets the coordinates of the player
        {
            if (!HasThePlayerAlreadySpawned())
                throw new InvalidOperationException("There is no player on the table!");

            Point coord = new Point();
            for (int i = 0; i < _fieldPlayer.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldPlayer.GetLength(1); j++)
                {
                    if (_fieldPlayer[i, j] == 1)
                    {
                        coord = new Point(i, j);
                    }
                }
            }

            return coord;
        }

        public bool IsThereAnyEnemiesOnTheTable() //checks if there is any enemies on the table
        {
            for (int i = 0; i < _fieldEnemies.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldEnemies.GetLength(1); j++)
                {
                    if (_fieldEnemies[i, j] == 2)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public Point[] GetEnemyCoordinates() //gets the coordinates of the enemiess
        {
            if (!IsThereAnyEnemiesOnTheTable())
                throw new InvalidOperationException("There is no any enemies on the table.");

            Point[] coordArray = new Point[PlayingEnemiesCount];
            Point coord = new Point();
            int z = 0;
            for (int i = 0; i < _fieldEnemies.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldEnemies.GetLength(1); j++)
                {
                    if (_fieldEnemies[i, j] == 2)
                    {
                        coord = new Point(i, j);
                        coordArray[z] = coord;
                        z++;
                    }
                }
            }

            return coordArray;
        }

        public bool IsThereAnyMinesOnTheTable() //checks if any mines have been spawned
        {
            for (int i = 0; i < _fieldMines.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldMines.GetLength(1); j++)
                {
                    if (_fieldMines[i, j])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<Point> GetMinesCoordinates() //gets the coordinates of mines
        {
            Point coord = new Point();
            List<Point> coordList = new List<Point>();
            for (int i = 0; i < _fieldMines.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldMines.GetLength(1); j++)
                {
                    if (_fieldMines[i, j])
                    {
                        coord = new Point(i, j);
                        coordList.Add(coord);
                    }
                }
            }
            return coordList;
        }

        //Game over methods:
        public Boolean HasAnyEnemiesCaughtPlayer() //checks if an enemy caught the player 
        {
            foreach (Point enemy in GetEnemyCoordinates())
            {
                if (enemy == PlayerCoordinates) { return true; }
            }
            return false;
        }
        public Boolean IsPlayerInAMine(int x, int y) //game is over. player lost
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");
            if (!IsPlayer(x, y))
                throw new Exception("Player is not on this field, querrying was unsuccessful.");

            return _fieldMines[x, y];
        }

        public Boolean IsAnEnemyInAMine(int x, int y) //if true and the count of enemies is 0, player won
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");
            if (!IsEnemy(x, y))
                throw new Exception("There is no any enemies on this field, querrying was unsuccessful.");

            return _fieldMines[x, y];
        }

        public Boolean AreAllEnemiesInMines() //checks if all enemies are in mines
        {
            int count = 0;
            for (int i = 0; i < _fieldEnemies.GetLength(0); i++)
            {
                for (int j = 0; j < _fieldEnemies.GetLength(1); j++)
                {
                    if (IsEnemy(i,j))
                    {
                        if (IsAnEnemyInAMine(i, j)) { count++; }
                    }
                }
            }
            return count == PlayingEnemiesCount;
        }
        public void TakingOutFallenEnemy(int x, int y)
        //removes enemies who have fallen into mines
        {
            if (x < 0 || x >= Size)
                throw new ArgumentOutOfRangeException(nameof(x), "X is out of range.");
            if (y < 0 || y >= Size)
                throw new ArgumentOutOfRangeException(nameof(y), "Y is out of range.");
            if (!IsEnemy(x, y))
                throw new Exception("There is no any enemies on this field, removing was unsuccessful.");
            if (!IsAnEnemyInAMine(x, y))
                throw new Exception("Enemy is not in a mine, removing was unsuccessful.");

            _fieldEnemies[x, y] = 0;
        }
        #endregion

        #endregion

        #region Private methods

        private Point EnemySteppingStratergy(Point enemy) //counts the best stepping stratergy for the enemies
        {           
            if (Math.Abs(PlayerCoordinates.X - enemy.X) > Math.Abs(PlayerCoordinates.Y - enemy.Y))
            {
                //the distance is larger on the x axis (horizontally), therefore enemy moves horizontally closer to the player
                if (Math.Abs(PlayerCoordinates.X - (enemy.X + 1)) < Math.Abs(PlayerCoordinates.X - (enemy.X - 1)))
                {
                    return new Point(1, 0);
                }
                else
                {
                    return new Point(-1, 0);
                }
            }
            else
            {
                if (Math.Abs(PlayerCoordinates.Y - (enemy.Y + 1)) < Math.Abs(PlayerCoordinates.Y - (enemy.Y - 1)))
                {
                    return new Point(0, 1);
                }
                else
                {
                    return new Point(0, -1);
                }
            }

        }
        #endregion
    }
}